package models

import (
	"database/sql"
	"fmt"
	"time"
)

// Define a Snippet type to hold the data for an individual snippet. Notice how
// the fields of the struct correspond to the fields in our MySQL snippets
// table?
type Snippet struct {
	ID      int    `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Created time.Time
	Expires time.Time `json:"expires"`
}

// Define a SnippetModel type which wraps a sql.DB connection pool.
type SnippetModel struct {
	DB *sql.DB
}

// This will insert a new snippet into the database.
func (m *SnippetModel) Insert(body Snippet) (int, error) {
	stmt := `insert into snippets(title, content, created, expires) values (?, ?, UTC_TIMESTAMP(),UTC_TIMESTAMP())`
	result, insert_err := m.DB.Exec(stmt, body.Title, body.Content)

	if insert_err != nil {
		return -1, insert_err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return -1, nil
	}
	return int(id), nil
}

// This will return a specific snippet based on its id.
func (m *SnippetModel) Get(id int) (*Snippet, error) {
	stmt := `select * from snippets where id = ?`
	result := m.DB.QueryRow(stmt, id)

	s := &Snippet{}
	err := result.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
	if err != nil {
		return nil, err
	}
	return s, nil
}

// This will return the 10 most recently created snippets.
func (m *SnippetModel) Latest() ([]*Snippet, error) {
	stmt := "select * from snippets order by id limit 10"

	rows, err := m.DB.Query(stmt)
	if err != nil {
		return nil, err
	}

	snippets := []*Snippet{}
	for rows.Next() {
		s := &Snippet{}
		mappingErr := rows.Scan(&s.ID, &s.Title, &s.Content, &s.Created, &s.Expires)
		if mappingErr != nil {
			fmt.Println(mappingErr)
		}

		snippets = append(snippets, s)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return snippets, nil
}
