package main

import (
	"encoding/json"
	"net/http"
	"snippet-box/web/db/models"
	"strconv"
)

func (app *Application) home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		app.notFound(w)
	} else {
		snippets, dbErr := app.snippets.Latest()
		if dbErr != nil {
			app.serverError(w, dbErr)
		}
		//for _, snippet := range snippets {
		//	fmt.Println(snippet)
		//}
		//fmt.Println(snippets)
		w.Header().Set("Content-Type", "application/json")
		encodingError := json.NewEncoder(w).Encode(snippets)
		//_, err := w.Write([]byte("Hello GO Service!\n"))
		//fmt.Println(r.Header.Values(""))
		if encodingError != nil {
			app.serverError(w, encodingError)
		}
	}
}

func (app *Application) view(w http.ResponseWriter, r *http.Request) {
	app.infoLog.Println("VIew called")
	id := r.URL.Query().Get("id")
	if id == "" {
		app.clientError(w, http.StatusBadRequest)
		return
	}
	dbId, err := strconv.Atoi(id)
	if err != nil {
		app.serverError(w, err)
		return
	}
	snippet, err := app.snippets.Get(dbId)
	w.Header().Set("Content-Type", "application/json")
	requestError := json.NewEncoder(w).Encode(snippet)
	if requestError != nil {
		app.serverError(w, requestError)
		return
	}

}

func (app *Application) create(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		app.clientError(w, http.StatusMethodNotAllowed)
		return
	}
	var request models.Snippet
	requestError := json.NewDecoder(r.Body).Decode(&request)
	if requestError != nil {
		app.serverError(w, requestError)
		return
	}
	app.infoLog.Println(&request)
	insertId, insertError := app.snippets.Insert(request)
	//encodingError := json.NewEncoder(w).Encode(request)
	//if encodingError != nil {
	//	app.serverError(w, requestError)
	//	return
	//}
	//_, err := w.Write([]byte("Create Service!\n"))
	//_, err := w.Write([]byte("Create Service!\n"))
	//fmt.Println(r.Header.Values(""))
	if insertError != nil {
		app.serverError(w, insertError)
		return
	}
	request.ID = insertId
	encodingError := json.NewEncoder(w).Encode(request)
	if encodingError != nil {
		app.serverError(w, requestError)
		return
	}
}
