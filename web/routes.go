package main

import "net/http"

func (app Application) routes() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/", app.home)
	mux.HandleFunc("/view", app.view)
	mux.HandleFunc("/create", app.create)

	return app.requestLogger(app.secureHeaders(mux))
}
