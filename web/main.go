package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
	"os"
	"snippet-box/web/db/models"
)

type Application struct {
	infoLog  *log.Logger
	errorLog *log.Logger
	snippets *models.SnippetModel
}

func main() {
	fmt.Println("Hello Go Service!")

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	db, _ := openDb("root:ungabunga123@/snippet_box?parseTime=true")

	app := &Application{errorLog: errorLog, infoLog: infoLog, snippets: &models.SnippetModel{db}}

	mux := app.routes()

	infoLog.Println("Starting server : 4000")

	err := http.ListenAndServe(":4000", mux)
	if err != nil {
		errorLog.Fatal(err)
	}
}

func openDb(dsn string) (*sql.DB, error) {
	db, sqlError := sql.Open("mysql", dsn)
	if sqlError != nil {
		return nil, sqlError
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	fmt.Println("Connected to database.")
	return db, nil
}
